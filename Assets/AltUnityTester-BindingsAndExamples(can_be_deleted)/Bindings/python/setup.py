from setuptools import setup

setup(name='altunityrun',
      version='1.2.2',
      description='Python Binding to allow Appium tests to be run against Unity games and apps using AltUnityTester',
      long_description='This package includes the Python bindings needed for tests to be run against Unity games and apps using AltUnityTester. \n\nFor more information, visit https://gitlab.com/altom/altunitytester',
      url='https://gitlab.com/Vingithub93/unityautomation',
      author='Unity Testing',
      author_email='vinayakpatil.sw@gmail.com',
      license='GNU General Public License v3.0',
      packages=['altunityrunner'],
      zip_safe=False)